class Coordinate(object):

    def __init__(self, x,y):
        self.x=x
        self.y=y

    def distance(self, other):
        x_diff_sq=(self.x-other.x)**2
        y_diff_sq=(self.y-other.y)**2
        return (x_diff_sq+y_diff_sq)**0.5

    def __str__(self):
        return "First num >> "+str(self.x) + "\nSecond num >> "+str(self.y)

a=Coordinate(3,4) 
b=Coordinate(0,1)
print(a.x)  # 3
print(b.y)  # 1
print(a.distance(b)) # a lot easier here :)
print(a)
# you the king of Python, that's it.


