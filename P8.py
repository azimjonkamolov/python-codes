class Coordinate(object):
    def __init__(self, x, y):
        self.x=x
        self.y=y

first=Coordinate(3,5)
second=Coordinate(1,2)
print(first.x)          # 3
print(second.y)         # 4
