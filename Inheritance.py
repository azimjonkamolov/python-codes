class Animal(object):
    def __init__(self, age):
        self.age=age
        self.name=None
    def get_age(self):
        return self.age

    def get_name(self):
        return self.name

    def set_age(self, newage):
        self.age=newage
    def set_name(self, newname=""):
        self.age=newname

    def __str__(self):
        return "Animal: " + str(self.name) + ": " +str(self.age)

class Cat(Animal):
    def speak(self):
        print("Meow")
    def __str__(self):
        return "Cat: " + str(self.name) + " : " +str(self.age)

class Person(Animal):
    def __init__(self, name, age):
        Animal.__init__(self,age)
        self.set_name(name)
        self.friends=[]
    def get_friends(self):
        return self.get_friends
    def add_friends(self, fname):
        if fname not in self.friends:
            self.friends.append(fname)
    def speak(self):
        print("Hello")
    def age_diff(self, other):
        diff=self.age-other.age
        print(abs(diff), "years difference")

    def __str__(self):
        return "person: " + str(self.name)+" : "+str(self.age)

import random

class Student(Person):
    def __init__(self, name, age, major=None):
        Person.__init__(self,name,age)
        self.major=major
    def change_major(self, major):
        self.major=major
    def speak(self):
        r=random.random()
        if r < 0.25:
            print("I have homework")
        elif 0.25 <= r <=0.5:
            print("I need to sleep")
        elif 0.5 <= r <= 0.75:
            print("I should eat")
        else:
            print("I am watching tv")

    def __str__(self):
        return "Student: " + str(self.name) + " : " + str(self.age) + " : " + str(self.major)
